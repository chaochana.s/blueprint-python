from sample.modules.math_util import square

class TestClass:
    def test_square_int(self):
        assert square(10) == 100
    
    def test_square_float(self):
        assert square(1.2) == 1.44
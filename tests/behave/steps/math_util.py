from behave import given, when, then
from hamcrest import assert_that, equal_to
from sample.modules.math_util import square


@given("Square module")
def step_impl(context):
    print('Given step')
    assert context.failed is False


@when('calculate square of {square_input}')
def step_impl(context, square_input):
    print('When step')
    context.input = float(square_input)


@then('calculated square result is {square_result}')
def step_impl(context, square_result):
    print('Then step')
    assert_that(square(context.input), equal_to(float(square_result)))

Feature: Math Square

  Scenario: Square result of an integer
     Given Square module
      When calculate square of 10
      Then calculated square result is 100

  Scenario: Square result of a float
     Given Square module
      When calculate square of 1.2
      Then calculated square result is 1.44
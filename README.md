# Blueprint for Python

Skeleton of Python testing framework, consists of:
- Sample function code
- Pytest, Pytest-Cov & Coverage.py
- behave BDD
- Gitlab CI/CD Configuration: JUnit test report, Code Coverage, and Code Quality
#### Getting Started on your machine by *'# pipenv install'* after clone

#### Pytest & Coverage.py
The pytest framework makes it easy to write small, readable tests, and can scale to support complex functional testing for applications and libraries.

Default setting of pytest runner, test script file name is required to be align with test_*.py or *_test.py. For this example, I named it `test_square.py`. The test is straightforward to assert result of the written math function.

``` Python
def test_square_int(self):
    assert square(10) == 100
```

Basic running command:
``` bash
$ pipenv run pytest
```

In CI, we need two more things, junit report and Cobertura coverage report
``` yml
unittest:
  stage: test
  script:
    - pipenv run coverage run -m pytest --junitxml=reports/unittest_junit.xml
    - pipenv run coverage report
    - pipenv run coverage xml -oreports/coverage.xml
  artifacts:
    when: always
    paths:
      - reports/
    expire_in: 1 week
    reports:
      junit: reports/unittest_junit.xml
      cobertura: reports/coverage.xml
```

#### behave BDD
behave is behaviour-driven development (BDD), Python style.
Basically BDD consists of two parts; Feature & Step

**Feature file** is a business readable document which helps you to describe business behavior without going into details of implementation. Feature file is written with [Gherkin syntax](https://cucumber.io/docs/gherkin/).
```Gherkin
  Scenario: Square result of an integer
     Given Square module
      When calculate square of 10
      Then calculated square result is 100
```

**Step file** is Python code mapped to a line in Feature file.
In this example, [Hamcrest](https://github.com/hamcrest/PyHamcrest) (assert_that, equal_to, etc) is using as a matcher and asserter.

```Python
@then('calculated square result is {square_result}')
def step_impl(context, square_result):
    print('Then step')
    assert_that(square(context.input), equal_to(float(square_result)))
```

In CI Configuration, we need to enable junit report for test cases visibility, same as Pytest one.
```yml
test:
  stage: test
  script:
    - pipenv run behave tests/behave --junit
  artifacts:
    when: always
    paths:
      - reports/
    expire_in: 1 week
    reports:
      junit: reports/*.xml
```

#### Code Quality
Flake8 and Code Climate is being used in this example by [flake8-gl-codeclimate](https://github.com/awelzel/flake8-gl-codeclimate) library.

Here's configuration in CI. You can find Code Quality report in the Gitlab CI/CD-Pipelines.
```yml
flake8:
  stage: test
  script:
    - pipenv run flake8 --exit-zero --format gl-codeclimate --output-file reports/gl-code-quality-report.json sample
  artifacts:
    reports:
      codequality: reports/gl-code-quality-report.json
```

#### Coverage Badge
Add the regex in CI/CD Settings > General Pipelines > Test coverage parsing to:
```regex
^TOTAL.+?(\d+.\d+\%)$
```
